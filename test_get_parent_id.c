#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define STANDARD_INPUT 0
#define STANDARD_OUTPUT 1

int test_get_parent_id()
{
    int parent_id = get_parent_id();
    return parent_id;
}

int main()
{
    int child_id = fork();
    if(child_id == 0)
    {
        int parent_id = test_get_parent_id();
        printf(STANDARD_OUTPUT, "parent id of current process is %d\n", parent_id);
        exit();
        return 1;
    }
    else
    {
        wait();
        int pid = getpid();
        printf(STANDARD_OUTPUT,"my id as parent is %d\n", pid);
        exit();
        return 1;
    }
}