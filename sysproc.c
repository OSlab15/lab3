#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}


//return parent id of current process

int
sys_get_parent_id(void)
{
  return myproc()->parent->pid;
}

int
sys_get_children(void)
{
  int childrenIds = 0, parentId;

  if(argint(0, &parentId) < 0)
    return -1;

  childrenIds = get_children_from_ptable(parentId);
  return childrenIds;
}

int
sys_get_descendant(void)
{
  int parentId;

  if(argint(0, &parentId) < 0)
    return -1;

  return get_descendant_from_ptable(parentId);
}

int
sys_nap(void)
{
  int n;
  uint ticks0;
  struct proc *p = myproc();
  struct rtcdate timeBeforeSys, timeAfterSys;
  cmostime(&timeBeforeSys);
  if(argint(0, &n) < 0)
    return -1;
  n = n * 100;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(p->killed){
      release(&tickslock);
      return -1;
    }
    release(&tickslock);
    acquire(&tickslock);
  }
  release(&tickslock);  
  cmostime(&timeAfterSys);
  cprintf( "time before syscall: %d:%d:%d\ntime after syscall: %d:%d:%d\n",
          timeBeforeSys.hour ,timeBeforeSys.minute, timeBeforeSys.second, 
          timeAfterSys.hour ,timeAfterSys.minute, timeAfterSys.second);

  return 0;
}

int sys_set_lottery_ticket(void){
  int pid = 0, numOfLotteryTickets = 0, result = 0;
  if(argint(0, &pid) < 0)
    return -1;
  if(argint(1, &numOfLotteryTickets) < 0)
    return -1;
  result = get_process_withPid(pid, SET_LOTTERY, numOfLotteryTickets);
  if(!result){
    cprintf("Process with pid %d not found", pid);
    return -1;
  }
  return 0;
}

int sys_set_remained_priority(void){
  int pid = 0, result = 0;
  char* remainingPriority;
  if(argint(0, &pid) < 0)
    return -1;
  if(argstr(1, &remainingPriority) < 0)
    return -1;
  result = set_process_priority(pid, remainingPriority);
  if(!result){
    cprintf("Process with pid %d not found", pid);
    return -1;
  }
  return 0;
}

int sys_change_queue(void){
  int pid = 0, queueNum = 0, result = 0;
  if(argint(0, &pid) < 0)
    return -1;
  if(argint(1, &queueNum) < 0)
    return -1;
  result = get_process_withPid(pid, CHANGE_QUEUE, queueNum);
  if(!result){
    cprintf("Process with pid %d not found", pid);
    return -1;
  }
  return 0;
}

int sys_print_processes(void){
  print_all_processes();
  return 0;
}