#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define STANDARD_INPUT 0
#define STANDARD_OUTPUT 1
#define CHILDREN_NUM 4

int test_get_children(int parent_id)
{
    return get_children(parent_id);
}

int main()
{
    int my_pid, i, systemcall_children_ids, is_child = 0;
    int children_ids[CHILDREN_NUM];
    children_ids[0] = fork();
    for(i = 1; i < CHILDREN_NUM; i++ )
        if(children_ids[i - 1] != 0)
        {
            children_ids[i] = fork();
        }
        else
        {
            is_child = 1;
            exit();
        }
    
    if(children_ids[CHILDREN_NUM - 1] == 0)
    {
        is_child = 1;
        exit();
    }
    else
    {
    }
    if(!is_child)
    {
        my_pid = getpid();
        systemcall_children_ids = get_children(my_pid);
        printf(STANDARD_OUTPUT, "systemcall result is %d\n", systemcall_children_ids);
        printf(STANDARD_OUTPUT, "my childs are ");
        for(i = 0; i < CHILDREN_NUM; i++)
            printf(STANDARD_OUTPUT, "%d ", children_ids[i]);
        printf(STANDARD_OUTPUT, "\n");
        
        for(i = 0; i < CHILDREN_NUM; i++)
            wait();
    }
    exit();
}