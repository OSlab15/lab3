#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int main(int argc, char const *argv[])
{
    int numOfProcesses, i;
    if(argc < 2)
        numOfProcesses = 10;
    else
        numOfProcesses = atoi(argv[1]); 

    for(i = 0; i < numOfProcesses; i++)
    {
        if(fork() == 0)
        {
            while(1);
        }
        
    }
    
    for(i = 0; i < numOfProcesses; i++)
        wait();

    exit();
    return 0;
}