#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"
#include "date.h"

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);
char allPath[10][1000];
uint totalLotteryTickets = 0; 

static void wakeup1(void *chan);

unsigned long randstate = 1;
unsigned int rand()
{
  randstate = randstate * 1664525 + 1013904223;
  return randstate;
}

uint timeToSeconds(struct rtcdate* time){
  uint seconds = time->hour * 3600 + time->minute * 60 + time->second;
  return seconds;
}

int power(int base, int pow){
  int result = 1;
  for(int i = 0; i < pow; i++)
    result *= base;
  return result;
}

void floatToStr(float f, char* str){
	int a, b, c, k, l = 0, m, i = 0;
	
	if(f < 0.0){
		*(str + i) = '-';
    i++;
		f *= -1;
	}
	
	a = f;	
	f -= a;	
	k = 2;
	
  if(a == 0){
    *(str + i) = 48;
    i++;
  }
	while(k > -1){
		l = power(10, k);
		m = a / l;
		if(m > 0)
			break;
	  k--;
	}
	
	for(l = k + 1; l > 0; l--){
		b = power(10, l - 1);
		c = a / b;
		*(str + i) = c + 48;
    i++;
		a %= b;
	}
	*(str + i) = '.';
  i++;
	
	for(l = 0; l < 2; l++){
		f *= 10.0;
		b = f;
		str[i++] = b + 48;
		f -= b;
	}
	*(str + i) = '\0';
  i++;
}

float strTofloat(char* str){
  float result = 0, fact = 1;
  if (*str == '-'){
    str++;
    fact = -1;
  }
  for (int point_seen = 0; *str; str++){
    if (*str == '.'){
      point_seen = 1; 
      continue;
    }
    int d = *str - '0';
    if (d >= 0 && d <= 9){
      if (point_seen) fact /= 10.0f;
      result = result * 10.0f + (float)d;
    }
  }
  return result * fact;
}

void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
}

// Must be called with interrupts disabled
int
cpuid() {
  return mycpu()-cpus;
}

// Must be called with interrupts disabled to avoid the caller being
// rescheduled between reading lapicid and running through the loop.
struct cpu*
mycpu(void)
{
  int apicid, i;
  
  if(readeflags()&FL_IF)
    panic("mycpu called with interrupts enabled\n");
  
  apicid = lapicid();
  // APIC IDs are not guaranteed to be contiguous. Maybe we should have
  // a reverse map, or reserve a register to store &cpus[i].
  for (i = 0; i < ncpu; ++i) {
    if (cpus[i].apicid == apicid)
      return &cpus[i];
  }
  panic("unknown apicid\n");
}

// Disable interrupts so that we are not rescheduled
// while reading proc from the cpu structure
struct proc*
myproc(void) {
  struct cpu *c;
  struct proc *p;
  pushcli();
  c = mycpu();
  p = c->proc;
  popcli();
  return p;
}

//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
  struct proc *p;
  char *sp;
  struct rtcdate currentTime;

  acquire(&ptable.lock);

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;

  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;
  p->lotteryTickets = 10;
  totalLotteryTickets += p->lotteryTickets;
  p->queueNum = LOTTERY_QUEUE;
  p->remainingPriority = 5.0;
  p->executedCycleNumber = 1;
  cmostime(&currentTime);
  p->arrivalTime = timeToSeconds(&currentTime);
  
  release(&ptable.lock);
  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    p->state = UNUSED;
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;

  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;
  return p;
}

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];

  p = allocproc();
  
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");

  // this assignment to p->state lets other cores
  // run this process. the acquire forces the above
  // writes to be visible, and the lock is also needed
  // because the assignment might not be atomic.
  acquire(&ptable.lock);

  p->state = RUNNABLE;

  release(&ptable.lock);
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  struct proc *curproc = myproc();

  sz = curproc->sz;
  if(n > 0){
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  curproc->sz = sz;
  switchuvm(curproc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  int i, pid;
  struct proc *np;
  struct proc *curproc = myproc();

  // Allocate process.
  if((np = allocproc()) == 0){
    return -1;
  }

  // Copy process state from proc.
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
    kfree(np->kstack);
    np->kstack = 0;
    np->state = UNUSED;
    return -1;
  }
  np->sz = curproc->sz;
  np->parent = curproc;
  *np->tf = *curproc->tf;
  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(curproc->ofile[i])
      np->ofile[i] = filedup(curproc->ofile[i]);
  np->cwd = idup(curproc->cwd);

  safestrcpy(np->name, curproc->name, sizeof(curproc->name));

  pid = np->pid;

  acquire(&ptable.lock);

  np->state = RUNNABLE;

  release(&ptable.lock);
  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
  struct proc *curproc = myproc();
  struct proc *p;
  int fd;

  if(curproc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(curproc->ofile[fd]){
      fileclose(curproc->ofile[fd]);
      curproc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(curproc->cwd);
  end_op();
  curproc->cwd = 0;
  if(curproc->queueNum == LOTTERY_QUEUE)
    totalLotteryTickets -= curproc->lotteryTickets;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(curproc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == curproc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  curproc->state = ZOMBIE;
  sched();
  panic("zombie exit");
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
  struct proc *p;
  int havekids, pid;
  struct proc *curproc = myproc();
  
  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for exited children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != curproc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        p->state = UNUSED;
        release(&ptable.lock);
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || curproc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(curproc, &ptable.lock);  //DOC: wait-sleep
  }
}

struct proc* lotteryScheduler(){
  struct proc* p;
  struct proc procTemp;
  p = &procTemp;
  unsigned int randNum;

  totalLotteryTickets = 0;
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  {
    if(p->queueNum == LOTTERY_QUEUE && p->state == RUNNABLE){
      totalLotteryTickets += p->lotteryTickets;
    }
  }

  if(totalLotteryTickets != 0 && totalLotteryTickets != 1)
    randNum = rand() % totalLotteryTickets;
  else if(totalLotteryTickets == 1)
    randNum = 0;
  else 
    return FAILED;
  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->queueNum == LOTTERY_QUEUE && p->state == RUNNABLE){
      if(randNum < p->lotteryTickets){
        return p;
      }
      else
        randNum -= p->lotteryTickets;    
    }
  }
  return FAILED;
}

struct proc* HRRNScheduler(){
  struct proc* p;
  struct proc procTemp;
  p = &procTemp;
  struct rtcdate currentTime;
  uint currentTimeSecs;
  struct proc* pIterator;
  float HRRNRatio;
  float maxHRRNRatio = -1;
  cmostime(&currentTime);
  currentTimeSecs = timeToSeconds(&currentTime);

  for(pIterator = ptable.proc; pIterator < &ptable.proc[NPROC]; pIterator++){
    if(pIterator->queueNum == HRRN_QUEUE && pIterator->state == RUNNABLE){
      HRRNRatio = ((float)(currentTimeSecs - pIterator->arrivalTime)) / ((float) pIterator->executedCycleNumber);
      if(HRRNRatio > maxHRRNRatio){
        maxHRRNRatio = HRRNRatio;
        p = pIterator;
      }
    }
  }

  if(maxHRRNRatio == -1)
    return FAILED;

  return p;
}

struct proc* SRPFScheduler(){
  struct proc* p;
  struct proc procTemp;
  p = &procTemp;
  int minPriority;
  int numOfProcsWithMinPriority = 0;
  struct proc* minPriorityProcesses[NPROC];
  unsigned int randNum;
  p = ptable.proc;
  minPriority = p->remainingPriority;
  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == RUNNABLE && p->queueNum == SRPF_QUEUE){
      if(p->remainingPriority == minPriority){
        minPriorityProcesses[numOfProcsWithMinPriority++] = p;
      }
      else if(p->remainingPriority < minPriority){
        numOfProcsWithMinPriority = 0;
        minPriorityProcesses[numOfProcsWithMinPriority++] = p;
        minPriority = p->remainingPriority;
      }
    }
    
  }

  if(numOfProcsWithMinPriority == 0)
    return FAILED;

  if(numOfProcsWithMinPriority != 1)
    randNum = rand() % numOfProcsWithMinPriority;
  else
    randNum = 0;  
    
  p = minPriorityProcesses[randNum];
  p->remainingPriority -= 0.1;
  if(p->remainingPriority < 0)
    p->remainingPriority = 0;

  return p;
}

void increaseTicketsForShell()
{
  struct proc temp;
  struct proc* p = &temp;
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  {
    if(p->pid == SHELL)
      p->lotteryTickets = 5000;
    struct proc* p1 = p->parent;
    if(p->pid != UNKNOWN && p->pid != INIT && p1 != 0 && p1->pid == SHELL)
    {
      p->lotteryTickets = 5000;
    }
  }
}

//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
{
  struct proc temp;
  struct proc *p = &temp;
  struct cpu *c = mycpu();
  c->proc = 0;
  
  for(;;){
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    increaseTicketsForShell();
    p = lotteryScheduler();
    if(p == FAILED){
      p = HRRNScheduler();
      if(p == FAILED){
        p = SRPFScheduler();
        if(p == FAILED){
        release(&ptable.lock);
        continue;
        }
      }
    }

    c->proc = p;
    switchuvm(p);
    p->state = RUNNING;
    p->executedCycleNumber += 1;
    swtch(&(c->scheduler), p->context);
    switchkvm();
    c->proc = 0;

    release(&ptable.lock);
  }
}

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state. Saves and restores
// intena because intena is a property of this
// kernel thread, not this CPU. It should
// be proc->intena and proc->ncli, but that would
// break in the few places where a lock is held but
// there's no process.
void
sched(void)
{
  int intena;
  struct proc *p = myproc();

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(mycpu()->ncli != 1)
    panic("sched locks");
  if(p->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = mycpu()->intena;
  swtch(&p->context, mycpu()->scheduler);
  mycpu()->intena = intena;
}

// Give up the CPU for one scheduling round.
void
yield(void)
{
  acquire(&ptable.lock);  //DOC: yieldlock
  myproc()->state = RUNNABLE;
  sched();
  release(&ptable.lock);
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot
    // be run from main().
    first = 0;
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }

  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
  struct proc *p = myproc();
  
  if(p == 0)
    panic("sleep");

  if(lk == 0)
    panic("sleep without lk");

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
    acquire(&ptable.lock);  //DOC: sleeplock1
    release(lk);
  }
  // Go to sleep.
  p->chan = chan;
  p->state = SLEEPING;

  sched();

  // Tidy up.
  p->chan = 0;

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
    release(&ptable.lock);
    acquire(lk);
  }
}

//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  static char *states[] = {
  [UNUSED]    "unused",
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [RUNNABLE]  "runble",
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie"
  };
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}

int get_children_from_ptable(int parentId)
{
  int childrenIds = 0;
  struct proc* p;
  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
  {
    if(p->state != UNUSED)
    {
      if(p->parent->pid == parentId)
      {
        childrenIds = childrenIds * 10 + p->pid;
      }
    }
  }
  release(&ptable.lock);
  return childrenIds;
}

int get_descendant_from_ptable(int parentId)
{
  int descendantIds = 0, numOfPossibleParents, curParentId;
  struct proc* p;
  int possibleParentIds[20];

  possibleParentIds[0] = parentId;
  numOfPossibleParents = 1;

  acquire(&ptable.lock);

  while(numOfPossibleParents != 0)
  {
    curParentId = possibleParentIds[numOfPossibleParents - 1];
    numOfPossibleParents--;

    cprintf("\ncurParentId = %d\n", curParentId);

    if(curParentId != parentId)
      descendantIds = descendantIds * 10 + curParentId;

    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    {
      if(p->state != UNUSED)
      {
        if(p->parent->pid == curParentId)
        {
          cprintf("pid of child is %d\n", p->pid);
          possibleParentIds[numOfPossibleParents] = p->pid;
          numOfPossibleParents++;
        }
      }
    }
  }

  release(&ptable.lock);
  return descendantIds;
}

int get_process_withPid(int pid, enum Mode mode, int newVal){
  struct proc temp;
  struct proc* process = &temp;
  acquire(&ptable.lock);
  for(process = ptable.proc; process < &ptable.proc[NPROC]; process++){
      if(process->pid == pid){
        switch(mode){
          case CHANGE_QUEUE:{
            if(process->queueNum == LOTTERY_QUEUE && newVal != LOTTERY_QUEUE){
              totalLotteryTickets = totalLotteryTickets - process->lotteryTickets;
            }
            else if(process->queueNum != LOTTERY_QUEUE && newVal == LOTTERY_QUEUE){
              totalLotteryTickets = totalLotteryTickets + process->lotteryTickets;
            }
            process->queueNum = newVal;
            break;
          }
          case SET_LOTTERY:{
            if(process->queueNum == LOTTERY_QUEUE)
              totalLotteryTickets = totalLotteryTickets - process->lotteryTickets + newVal;
            process->lotteryTickets = newVal;
            break;
          }
          default:;
        }
        release(&ptable.lock);
        return 1;
      }
  }
  release(&ptable.lock);
  return 0;
}
int set_process_priority(int pid, char* priorityStr){
  struct proc temp;
  struct proc* process = &temp;
  float priorityFloat = strTofloat(priorityStr);
  acquire(&ptable.lock);
  for(process = ptable.proc; process < &ptable.proc[NPROC]; process++){
      if(process->pid == pid){
        process->remainingPriority = priorityFloat;
        release(&ptable.lock);
        return 1;
      }
  }
  release(&ptable.lock);
  return 0;
}

int print_all_processes(void){
  struct proc* process;
  struct rtcdate currentTime;
  uint currentTimeSecs;
  float HRRNRatio;
  char HRRNRatioStr[20];
  char priorityStr[20];
  cprintf("name\t\tpid\t  state\t      queue   priority lottery cycles\tHRRN\n");
  cprintf("-------------------------------------------------------------------------------\n");
  acquire(&ptable.lock);
  for(process = ptable.proc; process < &ptable.proc[NPROC]; process++){
      if(!process->pid)
        continue;
      cmostime(&currentTime);
      currentTimeSecs = timeToSeconds(&currentTime);
      HRRNRatio = ((float)(currentTimeSecs - process->arrivalTime)) / ((float) process->executedCycleNumber);
      if(strlen(process->name) < 8)
        cprintf("%s\t\t", process->name);
      else if(strlen(process->name) < 16)
        cprintf("%s\t", process->name);
      else
        cprintf("%s", process->name);
      cprintf("%d\t", process->pid);
      switch(process->state){
        case UNUSED: cprintf("UNUSED  \t");
                      break;
        case EMBRYO: cprintf("EMBRYO  \t");
                      break;
        case SLEEPING: cprintf("SLEEPING\t");
                      break;
        case RUNNABLE: cprintf("RUNNABLE\t");
                      break;
        case RUNNING: cprintf("RUNNING \t");
                      break;
        case ZOMBIE: cprintf("ZOMBIE  \t");
        default:;
      }
      cprintf("%d\t", process->queueNum);
      floatToStr(process->remainingPriority, priorityStr);
      cprintf("%s\t", priorityStr);
      cprintf("%d\t", process->lotteryTickets);
      cprintf("%d\t", process->executedCycleNumber);
      floatToStr(HRRNRatio, HRRNRatioStr);
      cprintf("%s\n", HRRNRatioStr);
  }
  release(&ptable.lock);
  return 0;
}