#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define STANDARD_INPUT 0
#define STANDARD_OUTPUT 1
#define DESCENDANT_NUM 6


int test_get_descendant(int parentId)
{
    return get_descendant(parentId);
}

void forkDescendant(int* descendant)
{
    fork();
    fork();
}

int main()
{
    int actualDescendantIds[DESCENDANT_NUM];
    int myPid;
    forkDescendant(actualDescendantIds);
    myPid = getpid();
    
    int systemcalDescendantIds = test_get_descendant(myPid);
    printf(STANDARD_INPUT, "%d has descendant with ids = %d\n", myPid, systemcalDescendantIds);
    if(myPid == 4)
        sleep(1000);
    return 0;
}